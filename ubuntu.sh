clear

echo "Upgrading Packages..."
sudo apt update -y
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt get upgrade -y
echo "Installing Base Packages..."
sudo apt install zsh fzf curl unzip tmux git
mkdir -p ~/bin

clear

read -p "Would you like to install neovim for all users or just you? (all/me)" nvimallme

if [ $nvimallme == "all" ]
    then
    echo "The script will install neovim for all users."
    sudo curl -L https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -o /bin/nvim
    sudo chmod +x /bin/nvim  
fi
if [ $nvimallme == "me" ]
    then
    echo "The script will install neovim for just you."
    mkdir -p ~/bin
    curl -L https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -o ~/bin/nvim
    chmod +x ~/bin/nvim

fi

echo "Installing ZSH Plugin Manager..."
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh

read -p "Please specify a configuration repository. Default is https://gitlab.com/creepinson/my-config, We recommend you fork this and modify it: " repository
read -p "Please specify the git checkout point (Custom repository only!):" checkout
read -p "Please specify an installation script (Custom repository only!):"

echo "Installing .zshrc Configuration..."
if [ -z ${repository} ]
then
    git clone https://gitlab.com/creepinson/my-config ~/my-config
    cd ~/my-config
    git checkout zsh
    ./scripts/init.sh
else
    git clone $repository ~/my-config
    cd ~/my-config
    git checkout $checkout
    ./scripts/init.sh  
fi
