# Ubuntu 20.04 Server Setup

## Upgrading Apt Packages

```bash
sudo apt update -y
# Upgrade all installed packages to their latest versions
sudo apt upgrade -y
```

## Installing Base Packages

Next, I install the base packages that are required for the next steps to work.

```bash
sudo apt install zsh fzf curl unzip tmux git
```

## Installing Neovim

Now we can install the nightly version of neovim, a powerful command-line text editor.

```bash
mkdir -p ~/bin
curl -L https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -o ~/bin/nvim
# Make neovim runnable
chmod +x ~/bin/nvim
```

## Installing ZSH Plugin Manager

```bash
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
```

## Cloning My Configuration

```bash
git clone https://gitlab.com/creepinson/my-config ~/my-config
cd ~/my-config
git checkout zsh
./scripts/init.sh
```

## Post Installation

Now that everything is up and running, we can head to [the post install section](post-install.md).
