# Server Post Installation

## Changing The Default Shell

We can *optionally* change our default shell to zsh. The following command will prompt for your password.

```bash
chsh -s $(which zsh)
```

Now you can exit out of the terminal and log back in, and you should see the beautiful new starship prompt.

## Installing Python 3.9

```bash
sudo apt update -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt install python3.9
# Verify that it is installed
python3.9 --version
```

## Installing a Node Version

As of now, I have been using [NodeJS](https://nodejs.org) 15.

```bash
n 15
```

## Installing Docker

Optionally, we can install docker and docker compose if our server machine supports it. 

```bash
curl -fsSL https://get.docker.com | sh
sudo usermod -aG docker $USER
sudo rm -r /usr/local/bin/com.docker.cli
curl -L https://raw.githubusercontent.com/docker/compose-cli/main/scripts/install/install_linux.sh | sh
export COMPOSE_VERSION=${COMPOSE_VERSION:-v2.0.0-beta.3}
mkdir -p ~/.docker/cli-plugins/
curl -L https://github.com/docker/compose-cli/releases/download/${COMPOSE_VERSION}/docker-compose-linux-amd64 -o ~/.docker/cli-plugins/docker-compose
chmod +x  ~/.docker/cli-plugins/docker-compose
```
